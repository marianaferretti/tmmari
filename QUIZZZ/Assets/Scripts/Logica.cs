﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;
    
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}

public class ClassPergunta : Quiz
{ // CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as respostas
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // Checar resposta correta
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS


// CLASS LÓGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    { // METODO START

        ClassPergunta p1 = new ClassPergunta("1. A Costura Criativa é a confecção de: ", 100);
        p1.AddResposta("Vestuário em geral",false);
        p1.AddResposta("Bonecas e brinquedos", false);
        p1.AddResposta("Flores e decoração", false);
        p1.AddResposta("Bolsas, acessórios e utensílios", true);

        ClassPergunta p2 = new ClassPergunta("2. É um estruturador", 100);
        p2.AddResposta("Isopor", false);
        p2.AddResposta("Nulon Dublado", true);
        p2.AddResposta("Tricoline", false);
        p2.AddResposta("Cartolina", false);

        ClassPergunta p3 = new ClassPergunta("3. Qual agulha é a mai usada na Costura Criativa?", 100);
        p3.AddResposta("12", false);
        p3.AddResposta("14", true);
        p3.AddResposta("16", false);
        p3.AddResposta("18", false);

        ClassPergunta p4 = new ClassPergunta("4. TNT  siginifica: ", 100);
        p4.AddResposta("Tensão Neutra Tecido", false);
        p4.AddResposta("Tecido Não Tinto", false);
        p4.AddResposta("Tecido Não Tecido", true);
        p4.AddResposta("Tingimento Não Tóxico", false);

        ClassPergunta p5 = new ClassPergunta("5. É uma máquina de costura meância", 100);
        p5.AddResposta("Jack F4", false);
        p5.AddResposta("Janome 3160", false);
        p5.AddResposta("Janome 2008", true);
        p5.AddResposta("Jack A4", false);

        ClassPergunta p6 = new ClassPergunta("6. Material térmico usado para a confecçção de lacheiras: ", 100);
        p6.AddResposta("Etaflon", true);
        p6.AddResposta("Lona", false);
        p6.AddResposta("Nylon 600.", false);
        p6.AddResposta("Manta R2", false);

        ClassPergunta p7 = new ClassPergunta("7. O vivo para bolsas é também conhecido como", 100);
        p7.AddResposta("Cordão encerado", false);
        p7.AddResposta("Fio roliço", false);
        p7.AddResposta("Cordonê", false);
        p7.AddResposta("Espaguete.", true);

        ClassPergunta p8 = new ClassPergunta("8. É uma ferramenta de Costura Criativa: ", 100);
        p8.AddResposta("Giz de Cera", false);
        p8.AddResposta("Estilete 3/4", false);
        p8.AddResposta("Terminal Duplo", false);
        p8.AddResposta("Abridor de Casa", true);

       


        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();


    } // FIM METODO START

    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }


    private void ExibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }
    }

    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            //Debug.Log("Resposta certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
                
        }
        else
        {
            //Debug.Log("Resposta errada!!!");
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }




}// FIM CLASS LOGICA
